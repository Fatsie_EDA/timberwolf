//TODO: Description
#ifndef OUTPUT_H
#define OUTPUT_H

void init( void );
void addCell( int celltype, char *cellname );
void addNet( char *signal );
void addEquiv( void );
void addUnEquiv( void );
void add_instance( void );
void set_bbox( INT left, INT right, INT bottom, INT top );
void start_pt( int x, int y );
void add_pt( int x, int y );
void processCorners( void );
void check_xloc( char *value );
void check_yloc( char *value );
void check_sideplace( char *side );
void set_pinname( char *pinname );
void check_pos( int xpos, int ypos );
void output( void );

#endif //OUTPUT_H
