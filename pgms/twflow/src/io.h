// TODO: Descriptions
#ifndef IO_H
#define IO_H

#include "globals.h"

void setErrorFlag( void );
void init( INT numobj );
void add_object( char *pname, INT node );
void add_pdependency( INT fromNode );
void add_path( char *pathname );
void set_file_type( BOOL type );
void add_fdependency( char *file );
void add_args( char *argument );
void add_box( INT l, INT b, INT r, INT t );
void start_edge( INT fromNode );
void add_line( INT x1, INT y1, INT x2, INT y2 );
ADJPTR findEdge( INT from, INT to, BOOL direction );
void process_arcs( void );
void unmark_edges( void );

#endif //IO_H
