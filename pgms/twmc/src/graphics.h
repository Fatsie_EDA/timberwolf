// TODO: Descriptions
#ifndef GRAPHICS_H
#define GRAPHICS_H

void initMCGraphics( INT argc, char *argv[], INT windowId );
void setGraphicWindow( void );
void set_graphic_context( INT context );
void process_graphics( void );
void draw_the_data( void );
void graphics_dump( void );

#endif //GRAPHICS_H
