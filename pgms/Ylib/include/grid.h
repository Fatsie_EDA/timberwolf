//TODO: Descriptions
#ifndef YGRID_H
#define YGRID_H

void YforceGrid( INT *x , INT *y );
void Ygridx( INT *x );
void Ygridy( INT *y );
void Ygridx_down( INT *x );
void Ygridy_down( INT *y );
void Ygridx_up( INT *x );
void Ygridy_up( INT *y );
void Ygrid_setx( INT x, INT offset );
void Ygrid_sety( INT y, INT offset );
void Ygrid_getx( INT *x, INT *offset );
void Ygrid_gety( INT *y, INT *offset );

#endif //YGRID_H
