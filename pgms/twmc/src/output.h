// TODO: Descriptions
#ifndef OUTPUT_H
#define OUTPUT_H

void output( FILE *fp );
void output_pads( FILE *fp );
void set_determine_side( BOOL flag );
void output_vertices( FILE *fp, CELLBOXPTR cellptr );
void create_sc_output( void );

#endif //OUTPUT_H
