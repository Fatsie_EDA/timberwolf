// TODO: Descriptions
#ifndef INITNETS_H
#define INITNETS_H

void init_nets( void );
void cleanup_nets( void );
void set_net_error( void );
void add_path( BOOL pathFlag, char *net );
void end_path( INT lower_bound, INT upper_bound, INT priority );
PATHPTR get_path_list( void );
INT get_total_paths( void );
void init_analog( char *net );
void set_cap_upper_bound( DOUBLE cap );
void set_res_upper_bound( DOUBLE res );
void set_net_type( INT net_type );
void set_max_voltage_drop( DOUBLE drop );
void add_common_pt( void );
void add2common( char *cell, char *pin );
void common_cap( char *cell, char *pin );
void common_res( char *cell, char *pin );
void start_net_capmatch( char *netname );
void add_net_capmatch( char *netname );
void start_net_resmatch( char *netname );
void add_net_resmatch( char *netname );

#endif //INITNETS_H
