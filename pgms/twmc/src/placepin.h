// TODO: Descriptions
#ifndef PLACEPIN_H
#define PLACEPIN_H

void placepin( INT cell, BOOL newVertFlag );
void set_up_pinplace( void );
void update_pins( BOOL initialFlag );
void set_pin_verbosity( BOOL flag );
INT *find_pin_sides( INT cell );
INT find_tile_side( INT center, INT loc, INT direction );
void init_wire_est( void );

#endif //PLACEPIN_H
