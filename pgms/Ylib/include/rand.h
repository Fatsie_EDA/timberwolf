/* ----------------------------------------------------------------- 
TODO: Descriptions
----------------------------------------------------------------- */
#ifndef YRAND_H
#define YRAND_H

#include <yalecad/base.h>

INT Yacm_random( void );
void Yset_random_seed( INT seed );
INT Yget_random_var( void );

/* mytime.c */
UNSIGNED_INT Yrandom_seed( void );

/* random number [0...INT_MAX] */
#define RAND            (Yacm_random() )
/* random number [0...limit] */
#define RANDOM(limit)   (Yacm_random() % (limit))
#define ROLL_THE_DICE() ((DOUBLE) RAND / (DOUBLE)0x7fffffff ) 

#endif
