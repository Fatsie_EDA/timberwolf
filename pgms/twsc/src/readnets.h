/* ----------------------------------------------------------------- 
FILE:	    readnets.h
DESCRIPTION:This file redefines global variable of yacc and lex so
	    that we can have more than one parser in TimberWolfMC.
CONTENTS:   macro redefinitions for parser in readnets.
DATE:	    Oct 19, 1988 
REVISIONS:  Mar 27, 1989 - added to TimberWolfSC also added PSETBOX def.
	    Thu Mar  7 02:43:49 EST 1991 - added more definitions
		for byacc.
----------------------------------------------------------------- */
/* *****************************************************************
"@(#) readnets.h (Yale) version 4.4 3/7/91"
***************************************************************** */
#ifndef __READNETS_H
#define __READNETS_H

#include "standard.h"

typedef struct psetrec {
    INT  member; /* integer for determining membership */
    INT  path ;  /* data */
    struct psetrec *next ;
} PSETBOX, *PSETPTR ; /* path set record */

void readnets( FILE * );
PATHPTR get_path_list( void );
void add_paths_to_cells( void );
INT get_total_paths( void );

#endif // __READNETS_H
