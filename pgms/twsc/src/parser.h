/* ----------------------------------------------------------------- 
"@(#) parser.h (Yale) version 4.5 9/7/90"
FILE:	    parser.h                                       
DESCRIPTION:definitions for parsing.
CONTENTS:   
DATE:	    Dec  8, 1989 
REVISIONS:  
----------------------------------------------------------------- */
#ifndef PARSER_H
#define PARSER_H

#include <yalecad/hash.h>

#ifdef PARSER_VARS
#define EXTERN 
#else
#define EXTERN extern 
#endif

/* cell types */
#define STDCELLTYPE       1
#define EXCEPTTYPE        2
#define PADTYPE           3
#define PORTTYPE          4
#define EXTRATYPE         5
#define PADGROUPTYPE      6
#define HARDCELLTYPE      7
/* pin types */
#define PINTYPE           1
#define PASS_THRU         2
#define SWAP_PASS         3

EXTERN SWAPBOX *swap_group_listG ;
EXTERN BOOL one_pin_feedthruG ;

EXTERN INT maxCellOG ;
EXTERN INT case_unequiv_pinG ;
EXTERN INT celllenG ;
EXTERN INT cells_per_clusterG ;
EXTERN INT cluster_widthG ;
EXTERN INT extra_cellsG ;
EXTERN INT *fixLRBTG ;
EXTERN INT last_pin_numberG ;
EXTERN INT num_clustersG ;
EXTERN BOOL swappable_gates_existG ;

EXTERN INT swap_netG ;
EXTERN INT totallenG ;
EXTERN DOUBLE *padspaceG ;

#undef EXTERN  

void set_error_flag( void );
void initialize_parser( void );
void addCell( char *, INT );
void add_tile( INT, INT, INT, INT );
void add_initial_orient( INT );
void add_swap_group( char * );
void add_pingroup( void );
void end_pingroup( void );
void add_pin( char *, char *, INT, INT, INT );
void add_equiv( char *, INT, INT, INT, BOOL );
void add_port( char *, char *, INT, INT, INT );
void add_legal_blocks( INT );
void set_mirror_flag( void );
void add_orient( INT );
void fix_placement( char *, INT, char *, INT );
void add_extra_cells( void );
void cleanup_readcells( void );
void not_supported( char * );
YHASHPTR get_net_table( void );
void add_eco( void );
void init_corners( void );
void add_corner( INT, INT );
void process_corners( void );
void add_padside( char * );
void add_sidespace( DOUBLE, DOUBLE );
void setPermutation( BOOL );
void set_old_format( char * );
void add2padgroup( char *, BOOL );
void end_padgroup( void );

#endif /* PARSER_H */
