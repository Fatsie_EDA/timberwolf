#include <pads.h>
void align_pads( void );
void calc_constraints( PADBOXPTR, INT, DOUBLE *, DOUBLE *, BOOL *, INT *, INT * );
void dimension_pads( void );
void orient_pads( void );
