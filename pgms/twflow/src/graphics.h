// TODO: Descriptions
#ifndef GRAPHICS_H
#define GRAPHICS_H

#include "globals.h"

void init_graphics( int argc, char *argv[], INT windowId );
void draw_the_data( void );
void process_graphics( void );
ADJPTR get_edge_from_user( OBJECTPTR obj, BOOL direction );
void graphics_set_object( INT object );
ADJPTR make_decision( OBJECTPTR obj, BOOL direction );

#endif //GRAPHICS_H
