/* ----------------------------------------------------------------- 
FILE:	    okmalloc.h                                       
CONTENTS:   macro definitions for memory manager.
DATE:	    Tue Mar  3 16:05:51 EST 1992
REVISIONS:  
----------------------------------------------------------------- */
#ifndef YOKMALLOC_H
#define YOKMALLOC_H

#ifndef lint
static char Yokmalloc_HId[] = "@(#) okmalloc.h version 1.1 3/5/92" ;
#endif

/* memory definitions - for portability and ease of use */
#ifndef MEM_DEBUG

#define NIL( type )		     (type) NULL
#define YMALLOC(n, els)              (els *) Ysafe_malloc((n)*sizeof(els))
#define YCALLOC(n, els)              (els *) Ysafe_calloc(n, sizeof(els))
#define YREALLOC(ar,n,els)           (els *) Ysafe_realloc(ar,(n)*sizeof(els))
#define YFREE(els)                   Ysafe_free(els)

#define YVECTOR_MALLOC(lo, hi, els)   (els *) Yvector_alloc(lo,hi,sizeof(els))
#define YVECTOR_CALLOC(lo, hi, els)   (els *) Yvector_calloc(lo,hi,sizeof(els))
#define YVECTOR_FREE(ar,lo)           Yvector_free( ar,lo,sizeof(* ar))
#define YVECTOR_REALLOC(ar,lo,hi,els) (els *) Yvector_realloc( ar,lo,hi,sizeof(els))

/* memory manager definitions - safety net between memory manager */
extern void Ysafe_free( void *ptr ) ;
extern void Ysafe_cfree( void *ptr ) ;
extern char *Ysafe_malloc( INT bytes ) ;
extern char *Ysafe_calloc( INT num_entries, INT bytes ) ;
extern char *Ysafe_realloc( void *ptr, INT bytes ) ;
extern char *Yvector_alloc( INT lo, INT hi, INT size ) ;
extern char *Yvector_calloc( INT lo, INT hi, INT size ) ;
extern char *Yvector_realloc( VOIDPTR array,INT lo,INT hi,INT size ) ;
extern void Yvector_free( VOIDPTR array, INT lo, INT size ) ;

#else /* MEM_DEBUG - memory debug functions */

#define NIL( type )		     (type) NULL
#define YMALLOC(n, els) \
    (els *) Ysafe_malloc((n)*sizeof(els),__FILE__,__LINE__)
#define YCALLOC(n, els) \
    (els *) Ysafe_calloc(n, sizeof(els),__FILE__,__LINE__)
#define YREALLOC(ar,n,els) \
    (els *) Ysafe_realloc(ar,(n)*sizeof(els),__FILE__,__LINE__)
#define YFREE(els) \
    Ysafe_free(els,__FILE__,__LINE__)

#define YVECTOR_MALLOC(lo, hi, els) \
    (els *) Yvector_alloc(lo,hi,sizeof(els),__FILE__,__LINE__)
#define YVECTOR_CALLOC(lo, hi, els)  \
    (els *) Yvector_calloc(lo,hi,sizeof(els), __FILE__,__LINE__)
#define YVECTOR_REALLOC(ar,lo,hi,els) \
    (els *) Yvector_realloc( ar,lo,hi,sizeof(els),__FILE__,__LINE__)
#define YVECTOR_FREE(ar,lo) \
    Yvector_free( ar,lo,sizeof(* ar),__FILE__,__LINE__)

/* memory manager definitions - safety net between memory manager */
extern void Ysafe_free( void *ptr,char *file,INT line ) ;
extern void Ysafe_cfree( void *ptr,char *file,INT line ) ;
extern char *Ysafe_malloc( INT bytes,char *file,INT line ) ;
extern char *Ysafe_calloc( INT num_entries, INT bytes,char *file,INT line ) ;
extern char *Ysafe_realloc( void *ptr, INT bytes,char *file,INT line ) ;
extern char *Yvector_alloc( INT lo, INT hi, INT size,char *file,INT line ) ;
extern char *Yvector_calloc( INT lo, INT hi, INT size,char *file,INT line ) ;
extern char *Yvector_realloc( VOIDPTR a,INT lo,INT h,INT s,char *f,INT l ) ;
extern void Yvector_free( VOIDPTR array, INT lo, INT size,char *f,INT l ) ;

#endif /* MEM_DEBUG */

/* In both cases, define these */
extern INT YgetCurMemUse( void ) ;
extern INT YgetMaxMemUse( void ) ;
extern INT YcheckMemObj( char *ptr ) ;
extern void YdebugMemory( INT flag ) ;
extern INT YcheckDebug( VOIDPTR where ) ;
extern void Yinit_memsize( INT memsize ) ;
extern void Ydump_mem( void ) ;
extern void Ypmemerror( char *message ) ;

#endif /* YOKMALLOC_H */
