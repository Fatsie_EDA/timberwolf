/* ----------------------------------------------------------------- 
FILE:	    prboard.c                                       
DESCRIPTION:print board routine
CONTENTS:   
DATE:	    Jan 30, 1988 
REVISIONS:  Jul 30, 1988 - fixed output for softcells and added
		gridding of cells for mighty interface.
	    Mon Jan  7 18:31:00 CST 1991 - don't grid pads.
----------------------------------------------------------------- */
#ifndef lint
static char SccsId[] = "@(#) prboard.c version 3.6 4/18/91" ;
#endif

#include "custom.h"
#include "findcost.h"
#include "prboard.h"
#include <yalecad/debug.h>
#include <yalecad/grid.h>

void grid_cells( void )
{

INT xcenter , ycenter ;
INT cell ;
INT old_left, old_bottom ;/* original xy center of cell before gridding */
INT delta_x, delta_y ;       /* delta to move cell lower left to grid */
INT left, bottom ;            /* sides of cell bounding box */
CELLBOXPTR cellptr ;
BOUNBOXPTR bounptr ;

    if(!(gridCellsG)){
	return ;
    }
    for( cell = 1 ; cell <= endsuperG ; cell++ ) {
	cellptr = cellarrayG[ cell ] ;
	if( cellptr->celltype == GROUPCELLTYPE ||
	    cellptr->celltype == SUPERCELLTYPE ) {
	    /* avoid these cell types */
	    continue ;
	}
	bounptr = cellptr->bounBox[cellptr->orient] ;
	xcenter = cellptr->xcenter ;
	ycenter = cellptr->ycenter ;

	left = bounptr->l ;
	bottom = bounptr->b ;

	/* add offset */
	old_left = left += xcenter ;
	old_bottom = bottom += ycenter ;

	/* grid cell data if requested */
	/* set lower left corner to grid */
	YforceGrid( &left, &bottom ) ;

	/* now modify center of cell coordinates */
	delta_x = left - old_left ;
	delta_y = bottom - old_bottom ;
	cellptr->xcenter += delta_x ;
	cellptr->ycenter += delta_y ;

    } /* end for loop */

    /* update all the costs */
    funccostG = findcost() ;
    
} /* end grid_cells */
