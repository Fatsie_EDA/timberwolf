
/****************************************************************************
 ****************************************************************************

	File   : heap.h
	Author : Ted Stanion
	Date   : Mon Apr 30 22:36:17 1990

	Abstract : Include file for heap.c

	Revisions :

	Futures : 

 ****************************************************************************
 ****************************************************************************/

#ifndef YHEAP_H
#define YHEAP_H

#ifndef lint
static char YHeap_SccsId[] = "@(#) heap.h version 1.3 7/11/91";
#endif

#include <yalecad/base.h>

/****************************************************************************

	Structure : heap
	Author    : Ted Stanion
	Date      : Mon Apr 30 22:57:04 1990

	Abstract : Top level data structure for heaps.

*****************************************************************************/

typedef struct heap {
  INT (*heap_cmp)(VOIDPTR,VOIDPTR);
  struct heap_el *top;
} YHEAP, *YHEAPPTR;


/****************************************************************************

	Macro  : heap_empty
	Author : Ted Stanion
	Date   : Tue May  1 16:40:02 1990

	Abstract : Returns TRUE if the heap is empty.

*****************************************************************************/

#define heap_empty(h) (((h)->top) ? FALSE : TRUE)


/************************************************************************
 *  									*
 *  Global Functions							*
 *  									*
 ************************************************************************/

extern YHEAPPTR Yheap_init(void);
extern YHEAPPTR Yheap_init_with_parms(INT (*fn)(VOIDPTR,VOIDPTR));
extern void Yheap_empty(YHEAPPTR);
extern void Yheap_free(YHEAPPTR);
extern void Yheap_insert(YHEAPPTR, VOIDPTR);
extern VOIDPTR Yheap_delete_min(YHEAPPTR);
extern VOIDPTR Yheap_top(YHEAPPTR);
extern YHEAPPTR Yheap_meld(YHEAPPTR, YHEAPPTR);
extern INT Yheap_cmp_num(INT, INT);
extern INT Yheap_cmp_ptr(VOIDPTR, VOIDPTR);
extern void Yheap_check_mem(void);
extern INT Yheap_verify(YHEAPPTR);

#endif /* YHEAP_H */
