/* ----------------------------------------------------------------- 
FILE:	    system.c                                       
DESCRIPTION:system routines
DATE:	    Apr 26, 1990 
REVISIONS:  May 12, 1990 - added move file and getenv.
----------------------------------------------------------------- */
#ifndef lint
static char SccsId[] = "@(#) system.c version 3.4 8/28/90" ;
#endif

#include <yalecad/file.h>
#include <yalecad/message.h>
#include <yalecad/program.h>
#include <yalecad/system.h>

INT Ysystem( program, abortFlag, exec_statement, abort_func )
char *program ;
BOOL abortFlag ;
char *exec_statement ;
INT  (*abort_func)( void ) ;
{
    INT status ;        /* return status from program */

    if( (status = system( exec_statement )) != 0 ){
	/* get status from exit routine */
	status = (status & 0x0000FF00) >> 8 ;/* return code in 2nd byte */
	/* now determine the program */

	sprintf( YmsgG, "Program %s returned with exit code:%" PRIINT "\n",program,
	    status );
	M( ERRMSG, NULL, YmsgG ) ;
	if( abort_func ){
	    (*abort_func)() ;
	}
	if( abortFlag ){
	    YexitPgm( PGMFAIL ) ; /* exit the program */
	}
	return( status ) ;
    } 
    return( 0 ) ;
} /* end Ysystem */

void YcopyFile( sourcefile, destfile )
char *sourcefile, *destfile ;
{
    sprintf( YmsgG, "/bin/cp %s %s", sourcefile, destfile ) ;
    Ysystem( "Ylib/YcopyFile", ABORT, YmsgG, NULL ) ;
} /* end Ycopyfile */

void YmoveFile( sourcefile, destfile )
char *sourcefile, *destfile ;
{
    sprintf( YmsgG, "/bin/mv %s %s", sourcefile, destfile ) ;
    Ysystem( "Ylib/YmoveFile", ABORT, YmsgG, NULL ) ;
} /* end Ycopyfile */

void Yrm_files( files )
char *files ;
{
    sprintf( YmsgG, "/bin/rm -rf %s", files ) ;
    Ysystem( "Ylib/Yrm_files", NOABORT, YmsgG, NULL ) ;
} /* end Ycopyfile */

char *Ygetenv( env_var )
char *env_var ;
{
    char *getenv() ;

    return( (char *) getenv( env_var ) ) ;

} /* end Ygetenv */
