/* ----------------------------------------------------------------- 
FILE:	    readcell.h
DESCRIPTION:This file redefines global variable of yacc and lex so
	    that we can have more than one parser in TSMC.
CONTENTS:   macro redefinitions for parser in readcells.
DATE:	    Feb 9 , 1990 
REVISIONS:  
----------------------------------------------------------------- */
/* *****************************************************************
static char SccsId[] = "@(#) readcell.h version 1.3 3/4/91" ;
***************************************************************** */
