// TODO: Descriptions
#ifndef PATHS_H
#define PATHS_H

void print_paths( void );
INT calc_incr_time( INT cell );
void update_time( INT cell );
INT calc_incr_time2( INT cella, INT cellb );
void update_time2( INT cella, INT cellb );
void init_path_set( void );
void add2path_set( INT  path );
PSETPTR enum_path_set( void );
void clear_path_set( void );
void init_net_set( void );
void add2net_set( INT  net );
void clear_net_set( void );
INT dcalc_full_penalty( void );

#endif //PATHS_H
