//TODO: Decsription
#ifndef GENROWS_H
#define GENROWS_H

void init_data_structures( void );
void set_feed_length( DOUBLE percent );
void process_tiles( void );
void check_tiles( void );
void print_blk_file( void );
void print_tiles( void );
void print_vertices( void );
BOOL read_vertices( FILE *fp, BOOL initial );
BOOL restore_state( FILE *fp );
void save_state( FILE *fp );
void process_vertices( void );
void build_macros( void );
void divide_tile( TILE_BOX *tile, INT horiz_line );
void divide_tilelr( TILE_BOX *tile, INT vert_line );
void get_core( INT *left, INT *bottom, INT *right, INT *top, BOOL tileFlag );
ROW_BOX *get_rowptr( void );
void set_minimum_length( INT length );
void set_row_separation( DOUBLE channel_sep_relative, INT channel_sep_absolute );
void set_spacing( void );
BOOL force_tiles( void );
void check_user_data( void );
void remakerows( void );
void recalculate( BOOL freepts );
void set_core( INT left, INT right, INT bottom, INT top );
INT projectX( INT tile1_left, INT tile1_right, INT tile2_left, INT tile2_right );
INT projectY( INT tile1_bot, INT tile1_top, INT tile2_bot, INT tile2_top );
void calculate_numrows( void );

#endif //GENROWS_H
