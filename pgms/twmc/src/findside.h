// TODO: Descriptions
#ifndef FINDSIDE_H
#define FINDSIDE_H

INT findside( PSIDEBOX  *pSideArray, CELLBOXPTR cellptr, INT x , INT y );
void loadside( PSIDEBOX  *pSideArray, INT side, DOUBLE factor );
void load_soft_pins( CELLBOXPTR ptr, PSIDEBOX *pSideArray );

#endif //FINDSIDE_H
