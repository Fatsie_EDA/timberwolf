/* ----------------------------------------------------------------- 
"@(#) string.h version 1.7 3/6/92"
FILE:	    string.h                                       
DESCRIPTION:Insert file for string library routines.
DATE:	    Mar 16, 1989 
REVISIONS:  Tue Oct 23 01:29:24 EDT 1990 - added string prototypes.
----------------------------------------------------------------- */
#ifndef YSTRING_H
#define YSTRING_H

#include <string.h>

#include <yalecad/base.h>

extern char *Ystrclone( char *str ) ;
/* 
Function:
    Clone a string by allocating memory.  User must free memory when done.
*/

extern char **Ystrparser( char *str, char *delimiters,INT *numtokens ) ;
/*
Function:
    This string parsing function uses strtok to break up the string
    into tokens delimited by the set of characters given in the delimiter
    string.  Numtokens is set to the number of parsed tokens.  The function
    returns a pointer to an array of all the tokens each of which has
    been terminated with EOS.  This function destroys the given string
    so remember to copy the string if you need the original string for
    future use.
*/

extern char *Yremove_lblanks( char *bufferptr ) ;
/* 
Function:
    Remove leading blanks and tabs from a string.
*/

#endif  /* YSTRING_H */
