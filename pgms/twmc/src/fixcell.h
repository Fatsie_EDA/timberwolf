// TODO: Descriptions
#ifndef FIXCELL_H
#define FIXCELL_H

VOID updateFixedCells( BOOL initializeFlag ) ;
VOID init_fixcell( INT left, INT bottom, INT right, INT top ) ;
VOID build_active_array( VOID ) ;
VOID build_soft_array( VOID ) ;
VOID determine_origin( INT *x, INT *y, char *left_not_right, char *bottom_not_top ) ;
VOID delete_fix_constraint( INT cell );

#endif //FIXCELL_H
