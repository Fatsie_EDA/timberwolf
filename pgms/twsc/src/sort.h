#ifndef __SORT_H
#define __SORT_H

#include "feeds.h"

typedef struct graph_edge_cost {
    SHORT node1 ;
    SHORT node2 ;
    INT cost ;
    INT channel ;
}
*EDGE_COST ,
EDGE_COST_BOX ;

INT compare_cost( EDGE_COST *, EDGE_COST * );
INT comparegdx( CHANGRDPTR *, CHANGRDPTR * );
INT comparetxpos( IPBOXPTR *, IPBOXPTR * );
INT comparenptr( FEED_SEG_PTR *, FEED_SEG_PTR * );
INT comparepinx( PINBOXPTR *, PINBOXPTR * );
INT comparex( INT *, INT * );
INT cmpr_sx( PINBOXPTR *, PINBOXPTR * );
INT cmpr_lx( PINBOXPTR *, PINBOXPTR * );
INT cmpr_sy( PINBOXPTR *, PINBOXPTR * );
INT cmpr_ly( PINBOXPTR *, PINBOXPTR * );

#endif //__SORT_H
