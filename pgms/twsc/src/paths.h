#include "readnets.h"
void print_paths( void );
INT calc_incr_time( INT );
void update_time( INT );
void update_time2( void );
INT calc_incr_time2( INT, INT );
void init_path_set( void );
void add2path_set( INT );
PSETPTR enum_path_set( void );
void clear_path_set( void );
void init_net_set( void );
void add2net_set( INT );
BOOL member_net_set( INT );
void clear_net_set( void );
#ifdef DEBUG
INT dcalc_full_penalty( INT );
INT dpath_len( INT, BOOL );
INT dprint_error( void );
#endif //DEBUG
DOUBLE calc_time_factor( void );
void calc_init_timeFactor( void );
