/* ----------------------------------------------------------------- 
FILE:	    readcell.h
DESCRIPTION:This file redefines global variable of yacc and lex so
	    that we can have more than one parser in TimberWolfSC.
CONTENTS:   macro redefinitions for parser in readcell.
DATE:	    Aug  7, 1988 
REVISIONS:  
----------------------------------------------------------------- */
/* *****************************************************************
   static char SccsId[] = "@(#) readcell.h version 4.2 9/7/90" ;
***************************************************************** */
#ifndef __READCELL_H
#define __READCELL_H

void readcell( FILE * );

#endif // __READCELL_H
