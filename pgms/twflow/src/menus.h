
/* TWmenu definitions */  
#define TWNUMMENUS		19
#define AUTO_REDRAW_ON		1
#define AUTO_REDRAW_OFF		2
#define CLOSE_GRAPHICS		3
#define COLORS		4
#define CONTINUE_PGM		5
#define DUMP_GRAPHICS		6
#define EXIT_PROGRAM		7
#define FULLVIEW		8
#define REDRAW		9
#define TELL_POINT		10
#define TRANSLATE		11
#define ZOOM		12
#define CANCEL		0
#define AUTOFLOW		13
#define EXECUTE_PGM		14
#define PICK_PGM		15
#define PROMPT_ON		16
#define PROMPT_OFF		17
#define CANCEL		0


static TWMENUBOX menuS[20] = {
    {"CONTROL",NULL,0,1,0,0},
    {"Auto Redraw On","Auto Redraw Off",1,0,1,2},
    {"Close Graphics",NULL,0,0,3,0},
    {"Colors",NULL,0,0,4,0},
    {"Continue Pgm",NULL,0,0,5,0},
    {"Dump Graphics",NULL,0,0,6,0},
    {"Exit Program",NULL,0,0,7,0},
    {"FullView",NULL,0,0,8,0},
    {"Redraw",NULL,0,0,9,0},
    {"Tell Point",NULL,0,0,10,0},
    {"Translate",NULL,0,0,11,0},
    {"Zoom",NULL,0,0,12,0},
    {"Cancel",NULL,0,0,0,0},
    {"FLOW ",NULL,0,1,0,0},
    {"AutoFlow",NULL,0,0,13,0},
    {"Execute Pgm",NULL,0,0,14,0},
    {"Pick Pgm",NULL,0,0,15,0},
    {"Prompt On","Prompt Off",0,0,16,17},
    {"Cancel",NULL,0,0,0,0},
    {NULL,NULL,0,0,0,0}
} ;

