/* ----------------------------------------------------------------- 
FILE:	    readcgraph.h
DESCRIPTION:This file redefines global variable of yacc and lex so
	    that we can have more than one parser in TimberWolfMC.
CONTENTS:   macro redefinitions for graph parser in detail router..
DATE:	    Aug  7, 1988 
REVISIONS:  Thu Mar  7 01:41:07 EST 1991 - added more definitions
		for byacc.
----------------------------------------------------------------- */
/* *****************************************************************
"@(#) readcgraph.h (Yale) version 1.2 3/7/91"
***************************************************************** */
