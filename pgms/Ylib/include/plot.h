//TODO: Descriptions
#ifndef YPLOT_H
#define YPLOT_H

void Yplot_control( BOOL toggle );
void Yplot_init( int dval, ... );
void Yplot_heading( int dval, ... );
void Yplot_close( void );
void Yplot( int dval, ... );
void Yplot_flush( char *gName );

#endif //YPLOT_H
