/* ----------------------------------------------------------------- 
FILE:	    linalg.h
DESCRIPTION:Include file for linear algebra package.
CONTENTS:
DATE:       Tue Jan 15 01:29:36 EST 1991 - original coding.
REVISIONS:
----------------------------------------------------------------- */
#ifndef YLINALG_H
#define YLINALG_H

#ifndef lint
static char YlinAlgId[] = "@(#) linalg.h version 1.1 1/15/91" ;
#endif

#define YEPSILON  1.0E-12
#define YMIN     -1.0E-38
#define YZERO     0.0
#define YMATRIX( matrix, r, c )   matrix->m[r][c]

/* the matrix itself */
typedef struct {
    INT    rows ;
    INT    columns ;
    DOUBLE **m ;  
} YMBOX, *YMPTR ;

extern YMPTR Ymatrix_create( INT rows, INT columns ) ;
extern void Ymatrix_free( YMPTR mptr ) ;
extern YMPTR Ymatrix_transpose( YMPTR mptr ) ;
extern YMPTR Ymatrix_mult( YMPTR aptr, YMPTR bptr ) ;
extern YMPTR Ymatrix_sub( YMPTR aptr, YMPTR bptr ) ;
extern void Ymatrix_disp( YMPTR mptr ) ;
extern YMPTR Ymatrix_eye( INT size ) ;
extern void Ymatrix_zero( YMPTR matrix ) ;
extern YMPTR Ymatrix_copy( YMPTR input ) ;
extern YMPTR Ymatrix_linv( YMPTR aptr ) ;
extern YMPTR Ymatrix_cofactors( YMPTR aptr ) ;

#endif /* YLINALG_H */
