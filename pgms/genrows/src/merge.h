//TODO: Decsription
#ifndef MERGE_H
#define MERGE_H

void merge_tiles( void );
void merge_upward( TILE_BOX *begin_tile );
void merge_downward( TILE_BOX *begin_tile );
void merge_right( TILE_BOX *begin_tile );
void merge_left( TILE_BOX *begin_tile );
void renumber_tiles( void );

#endif //MERGE_H
