/* TODO: Description */
#ifndef _MENUS_H
#define _MENUS_H

void TWinforMenus( void );
void TWmouse_tracking_start( void );
BOOL TWmouse_tracking_pt( INT *x, INT *y );
BOOL TWmouse_tracking_end( void );
void TWfreeMenuWindows( void );

#endif //_MENUS_H
