// TODO: Descriptions
#ifndef WIREEST_H
#define WIREEST_H

void wireestxy( MOVEBOXPTR pos, INT xc, INT yc );
void wireestxy2( MOVEBOXPTR pos, INT xc, INT yc );
BOOL read_wire_est( FILE *fp );
void resize_wire_params( void );
void save_wireest( FILE *fp );
INT read_wireest( FILE *fp );

#endif //WIREEST_H
