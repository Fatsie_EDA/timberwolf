/* ----------------------------------------------------------------- 
FILE:	    readnets.h
DESCRIPTION:This file redefines global variable of yacc and lex so
	    that we can have more than one parser in TimberWolfMC.
CONTENTS:   macro redefinitions for parser in readnets.
DATE:	    Oct 19, 1988 
REVISIONS:  Mar 27, 1989 - added to TimberWolfSC also added PSETBOX def.
----------------------------------------------------------------- */
/* *****************************************************************
"@(#) readnets.h (Yale) version 3.1 3/21/90"
***************************************************************** */
typedef struct psetrec {
    int  member; /* integer for determining membership */
    int  path ;  /* data */
    struct psetrec *next ;
} PSETBOX, *PSETPTR ; /* path set record */
