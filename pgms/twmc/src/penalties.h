// TODO: Descriptions
#ifndef PENALTIES_H
#define PENALTIES_H

DOUBLE calc_lap_factor( DOUBLE percentDone );
DOUBLE calc_time_factor( DOUBLE percentDone );
DOUBLE calc_core_factor( DOUBLE percentDone );
DOUBLE calc_init_lapFactor( DOUBLE totFunc, DOUBLE totPen );
DOUBLE calc_init_timeFactor( DOUBLE avgdFunc, DOUBLE avgdTime );
DOUBLE calc_init_coreFactor( void );

#endif //PENALTIES_H
