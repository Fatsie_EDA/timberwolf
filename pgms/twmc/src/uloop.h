// TODO: Descriptions
#ifndef ULOOP_H
#define ULOOP_H

void uloop( INT limit );
void initStatCollection( void );
void getStatistics( DOUBLE *totalWire, DOUBLE *totalPenalty, DOUBLE *avg_time,
                    DOUBLE *avg_func );
void make_movebox( void );
void save_uloop( FILE *fp );
INT read_uloop( FILE *fp );
void set_dump_ratio( INT count );

#endif //ULOOP_H
