/* ----------------------------------------------------------------- 
FILE:	    readmver.h
DESCRIPTION:This file redefines global variable of yacc and lex so
	    that we can have more than one parser in TSMC.
CONTENTS:   macro redefinitions for parser in readmver.
DATE:	    Feb 9 , 1990 
REVISIONS:  
----------------------------------------------------------------- */
/* *****************************************************************
   static char SccsId[] = "@(#) readmver.h version 4.4 9/14/89" ;
***************************************************************** */
