// TODO: Descriptions
#ifndef WINDOW_H
#define WINDOW_H

DOUBLE eval_ratio( INT iteration );
void init_control( BOOL first );
void pick_position( INT *x, INT *y, INT ox, INT oy );
void pick_neighborhood( INT *x, INT *y, INT ox, INT oy, FIXEDBOXPTR fixptr );
void update_window_size( DOUBLE iteration );
void save_window( FILE *fp );
INT read_window( FILE *fp );

#endif //WINDOW_H
