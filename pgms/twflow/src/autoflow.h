// TODO: Descriptions
#ifndef AUTOFLOW_H
#define AUTOFLOW_H

#include "globals.h"

void auto_flow( void );
void exec_single_prog( void );
BOOL check_dependencies( ADJPTR adjptr );
void autoflow_set_object( INT object );

#endif //AUTOFLOW_H
