//TODO: Descriptions
#ifndef YSVD_H
#define YSVD_H

BOOL Ysvd_solve( YMPTR A, YMPTR B, YMPTR *Xret );
BOOL Ysvd_decompose( YMPTR A, YMPTR *Uret, YMPTR *Wret, YMPTR *Vret );

#endif //YSVD_H
