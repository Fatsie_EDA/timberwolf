// TODO: Descriptions
#ifndef PLACEPADS_H
#define PLACEPADS_H

void placepads( void );
#ifdef DEBUG
void print_pads( char *message, PADBOXPTR *array, INT howmany );
#endif
void setVirtualCore( BOOL flag );
void find_core_boundary( INT *left, INT *right, INT *bottom, INT *top );
void get_global_pos( INT cell, INT *l, INT *r, INT *b, INT *t );

#endif //PLACEPADS_H
