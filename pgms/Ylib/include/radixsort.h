#ifndef YRADIXSORT_H
#define YRADIXSORT_H

#include <yalecad/base.h>

extern int Yradixsort7( VOIDPTR *l1,INT n,UNSIGNED_INT endchar,
    VOIDPTR *tab, INT indexstart,char *(*ralloc)(), VOID (*rfree)() ) ;

extern int Yradixsort5( VOIDPTR *l1,INT n,UNSIGNED_INT endchar,VOIDPTR *tab,
    INT indexstart ) ;

extern int Yradixsort_pref( VOIDPTR *l1,INT n ) ;

extern int Yradixsort4( VOIDPTR *l1,INT n,UNSIGNED_INT endchar,VOIDPTR *tab );

extern int Yradixsort( VOIDPTR *l1,INT n,VOIDPTR *tab,UNSIGNED_INT endchar ) ;

extern int Yradixsort3( VOIDPTR *l1,INT n, UNSIGNED_INT endchar ) ;

extern char *Yradix_prefix( char *buffer, INT num ) ;

extern int Yradix_number( char *buffer ) ;

extern char *Yradix_suffix( char *buffer ) ;

extern char *Yradix_pref_clone( char *buffer ) ;

#endif

