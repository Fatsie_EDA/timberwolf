// TODO: Descriptions
#ifndef OVERLAP_H
#define OVERLAP_H

INT overlap( void );
INT overlap2( void );
void update_overlap( void );
void update_overlap2( void );
void turn_wireest_on( BOOL turn_on );
void setup_Bins( CELLBOXPTR s_cellptr, INT s_xc, INT s_yc, INT s_orient );
void add2bin( MOVEBOXPTR *cellpos );

#endif //OVERLAP_H
