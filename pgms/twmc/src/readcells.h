/* ----------------------------------------------------------------- 
FILE:	    custom_parser.h
DESCRIPTION:This file redefines global variable of yacc and lex so
	    that we can have more than one parser in TimberWolfMC.
CONTENTS:   macro redefinitions for parser in readcells.
DATE:	    Aug  7, 1988 
REVISIONS:  
----------------------------------------------------------------- */
/* *****************************************************************
   static char SccsId[] = "@(#) readcells.h version 3.4 3/6/92" ;
***************************************************************** */
#ifndef READCELLS_H
#define READCELLS_H

void readcells( FILE *fp );

#endif //READCELLS_H
