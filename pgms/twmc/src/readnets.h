/* ----------------------------------------------------------------- 
FILE:	    readnets.h
DESCRIPTION:This file redefines global variable of yacc and lex so
	    that we can have more than one parser in TimberWolfMC.
CONTENTS:   macro redefinitions for parser in readnets.
DATE:	    Oct 19, 1988 
REVISIONS:  Sun Dec 16 00:40:28 EST 1990 - moved net routines to 
		initnets.c and added analog functions.
	    Thu Mar  7 01:48:21 EST 1991 - added more definitions
		for byacc.
----------------------------------------------------------------- */
/* *****************************************************************
   static char SccsId[] = "@(#) readnets.h version 3.6 3/6/92" ;
***************************************************************** */
#ifndef READNETS_H
#define READNETS_H

#include "analog.h"
#define STARTPATH  1   /* flag for start of path */
#define CONTPATH   0   /* flag for continuing path */

void readnets( FILE *fp );

#endif //READNETS_H
