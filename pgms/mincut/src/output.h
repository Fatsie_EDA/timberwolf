// TODO: Descriptions
#ifndef OUTPUT_H
#define OUTPUT_h

#include <stdio.h>
#include <yalecad/base.h>

void init( void );
void addCell( int celltype, char *cellname );
void addNet( char *signal );
void set_bbox( INT left, INT right, INT bottom, INT top );
void output( FILE *fp );
void read_par( void );
void update_stats( FILE *fp );

#endif //OUTPUT_H
