// TODO: Descriptions
#ifndef CONFIGPADS_H
#define CONFIGPADS_H

void align_pads( void );
void calc_constraints( PADBOXPTR pad, INT side, DOUBLE *lb, DOUBLE *ub,
                       BOOL *spacing_restricted, INT *lowpos, INT *uppos );
void dimension_pads( void );
void orient_pads( void );

#endif //CONFIGPADS_H
